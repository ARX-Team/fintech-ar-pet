﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    public Text currencyText;
    public Button FoodButton;
    public Button WaterButton;
    public Button SleepButton;
    public Button AddPaymentButton;
    public Button ArButton;
    public Button backButton;
    public Gyro gyro;

    public MarkerLessAR markerLess;

	public InputField AddPaymentInput;

    public InventoryManager inventoryManager;
    public FintechManager fintechManager;
    public CurrencyController currencyManager;

	public Animator animator;


    public 

    // Use this for initialization
    void Start()
    {
        FoodButton.onClick.AddListener(() =>
        {
            inventoryManager.gift(treets.food, -10);
        });
        WaterButton.onClick.AddListener(() =>
        {
            inventoryManager.gift(treets.water, 10);
        });
        SleepButton.onClick.AddListener(() =>
        {
            inventoryManager.gift(treets.sleep, 10);
        });

        ArButton.onClick.AddListener(()=>{
            backButton.gameObject.SetActive(true);
            gyro.enabled = true;
            markerLess.enabled = true;
            markerLess.background.enabled = true;

        });

        backButton.onClick.AddListener(()=>{
            backButton.gameObject.SetActive(false);
            gyro.enabled = false;
            markerLess.enabled = false;
            gyro.transform.eulerAngles = new Vector3(15, 0 ,0);
            markerLess.background.enabled = false;
        });

		AddPaymentButton.onClick.AddListener(()=>{
			fintechManager.PostRequest("asdasd", null, ()=>{
				currencyManager.make_paymant(int.Parse(AddPaymentInput.text));
				animator.ResetTrigger("Add");
			});
		});
    }

	public void PlayFill(Animation anima)
	{
		anima.Stop();
		anima.Play();
	}


    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        currencyText.text = string.Format("I have saved\n<size=120><i>{0}$</i></size>\nfor you", currencyManager.Current_real_money);
    }

}
