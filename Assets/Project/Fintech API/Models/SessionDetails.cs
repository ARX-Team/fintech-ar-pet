namespace FintechAPI.Clients
{
	[System.Serializable]
	public class SessionDetails {

		public bool IsChannelActive;
        public int SessionTimeout;
	}
}