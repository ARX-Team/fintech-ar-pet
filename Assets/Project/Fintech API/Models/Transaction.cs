namespace FintechAPI.Transactions
{
    public class Transaction
    {
        public int AcctKey;
        public int EntryId;
        public string Nomination;
        public string EntryGroup;
        public string MerchantId;
        public int Date;
        public double Amount;
        public string Ccy;
        public string DocNomination;
        public string Beneficiary;
        public string DstAcc;
        public string SrcAcc;
        public string MerchantName;
        public string MerchantNameInt;
        public double AmountBase;
        public string EntryGroupName;
        public int EntryGroupNameId;
        public string ServiceId;
        public int PostDate;

    }
}