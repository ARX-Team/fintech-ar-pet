namespace FintechAPI.Clients
{
    public class Client
    {
        public string CategoryType;
        public string FirstName;
        public string LastName;
        public string FirstNameInt;
        public string LastNameInt;
        public string Sex;
        public int BirthDate;
        public string TranStatus;
        public string Resident;
        public string Pin;
        public string ClientCategory;
    }
}