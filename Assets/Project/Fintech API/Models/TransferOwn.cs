namespace FintechAPI.Transfers
{
    public class TransferOwn
    {
        public string SrcAcctKey;
        public string DstAcctKey;
        public double Amount;
        public string Ccy;
        public string Nomination;
        public string SrcCcy;
        public string DstCcy;
        public double Rate;
    }
}
