namespace FintechAPI.Products
{
    [System.Serializable]
    public class AvailableAmount
    {
        public double Amount;
        public string Currency;
    }
}