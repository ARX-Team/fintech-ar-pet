namespace FintechAPI.Products
{
    [System.Serializable]
    public class Account
    {
        public int AcctKey;
        public string AcctName;
        public string PrintAcctNo;
        public AvailableAmount[] AvailableAmounts;
        public string Ccy;
        public int OrderNo;
        public string ProductCode;
        public string Product;
        public int MainAcctKey;
        public string ProductGroup;
        public int ProductId;
        //SubAccounts
    }
}
