﻿namespace FintechAPI.Clients
{
    [System.Serializable]
    public class SessionInfo
    {

        public string SessionId;
        public SessionDetails SessionDetails;
        public UserDetails UserDetails;
    }
}