namespace FintechAPI.Products
{
    [System.Serializable]
    public class Deposit
    {
        public int AgreeKey;
        public string ProdType;
        public string Name;
        public string Ccy;
        public string DepositType;
        public int NextWithdrawalDate;
        public int AccountKey;
        public string AcctNo;
        public double CurrentBalance;
        public double AvailableAmount;
        public int ProductId;
        public string ProductGroup;
        public string DepositTypeName;
        public int MaturityDate;
        public int StartDate;
        public double IntAccrued;
        public string Status;
        public string CloseDate;
        public double InterestRate;
        public double TotalBalance;
    }
}