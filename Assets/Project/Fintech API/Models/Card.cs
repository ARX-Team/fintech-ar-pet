namespace FintechAPI.Products
{
    [System.Serializable]
    public class Card
    {
        public int Id;
        public int CardId;
        public int AcctKey;
        public int ClientKey;
        public string CardClass;
        public string CardName;
        public string CardType;
        public string ProductCode;
        public string SubProductCode;
        public string SubProductGroup;
        public string LastFour;
        public string CardHolder;
        public long ExpDate;
        public string IsCardBlocked;
        public string CardPan;
        public string CardForTypeDictionaryValue;
        public string ProductGroup;
        public int ProductId;
        public int SubProductId;
        public string NameDictionaryValue;
    }
}