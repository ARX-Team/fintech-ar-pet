﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerBase : MonoBehaviour
{


    public virtual void initilize()
    {
        Debug.Log("ControllerBase initilazed");
    }

    public virtual void refresh()
    {
        Debug.Log("ControllerBase refreshed");
    }
  



}
