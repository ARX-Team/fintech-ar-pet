﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using FintechAPI.Clients;
using FintechAPI.Products;
using FintechAPI.Transactions;

public class FintechManager : MonoBehaviour
{
    public void Login(string username, string password, System.Action<SessionInfo> action=null)
    {
        GetRequest<SessionInfo>(string.Format("https://api.fintech.ge/api/Clients/Login/{0}/{1}", username, password), action);
    }


    public void GetCardsInfo(string sessionId, System.Action<Cards> action)
    {
        GetRequest<Cards>(string.Format("https://api.fintech.ge/api/Products/Cards/{0}", sessionId), action);
    }

    public void GetCardInfo(string sessionId, int index, System.Action<Card> action)
    {
        GetRequest<Cards>(string.Format("https://api.fintech.ge/api/Products/Cards/{0}", sessionId), (data)=>action.Invoke(data.values[index]));
    }

    public void GetDepositsInfo(string sessionId, System.Action<Deposits> action)
    {
        GetRequest<Deposits>(string.Format("https://api.fintech.ge/api/Products/Deposits/{0}", sessionId), action);
    }

    public void GetDepositInfo(string sessionId, int index, System.Action<Deposit> action)
    {
        GetRequest<Deposits>(string.Format("https://api.fintech.ge/api/Products/Deposits/{0}", sessionId), (data)=>action.Invoke(data.values[index]));
    }

    public void GetAccountsInfo(string sessionId, System.Action<Accounts> action)
    {
        GetRequest<Accounts>(string.Format("https://api.fintech.ge/api/Products/Accounts/{0}", sessionId), action);
    }

    public void GetAccountInfo(string sessionId, int index, System.Action<Account> action)
    {
        GetRequest<Accounts>(string.Format("https://api.fintech.ge/api/Products/Accounts/{0}", sessionId), (data)=>action.Invoke(data.values[index]));
    }



    public void GetRequest<T>(string link, System.Action<T> action = null)
    {
        StartCoroutine(GetRequestEnumerator(link, action));
    }

    public void PostRequest(string link, Dictionary<string, string> data, System.Action action = null)
    {
        StartCoroutine(PostRequestEnumerator(link, data, action));
    }

    IEnumerator GetRequestEnumerator<T>(string link, System.Action<T> action = null)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(link))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                byte[] bytes = www.downloadHandler.data;
                string myjson = System.Text.Encoding.UTF8.GetString(bytes);
                if(myjson[1] == '{')
                    myjson = "{\"values\":" + myjson + "}";

                Debug.Log(myjson);
                T obj = JsonUtility.FromJson<T>(myjson);
                if(typeof(T) == typeof(Cards))
                    Debug.Log((obj as Cards).values);
                if (action != null)
                    action.Invoke(obj);
            }
        }
    }

    IEnumerator PostRequestEnumerator(string link, Dictionary<string, string> data, System.Action action = null)
    {
        using (UnityWebRequest www = UnityWebRequest.Post(link, data))
        {
                if(action != null)
                    action.Invoke();
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Upload Complete");
            }
        }
    }
}
