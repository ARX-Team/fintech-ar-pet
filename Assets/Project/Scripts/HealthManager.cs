﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthManager : ControllerBase
{

    [SerializeField]
    float maximum_water_limit;

    [SerializeField]
    float maximum_food_limit;

    [SerializeField]
    float maximum_sleep_limit;



    [SerializeField]
    AnimationMager animation_manager;

    [SerializeField]
    GameController game_controller;

    [SerializeField]
    CurrencyController currancy_controller;

    [SerializeField]
    float health_check_rate_in_seconds;

    [SerializeField]
    float vizualize_health_condition_rate;

    [SerializeField]
    float lion_baring; // maximum time animal can stay without outgoing paymant
    // is calculated such as
    // (currency_setisfaction_factor * payment_amount / last_playmant_time) + petting

    [SerializeField]
    float simulation_speed;


    [SerializeField]
    bool is_health_state_checked = false;

    public override void initilize() // this is called from gamemanager
    {
        base.initilize();
        animation_manager = FindObjectOfType<AnimationMager>();
        game_controller = FindObjectOfType<GameController>();
        currancy_controller = FindObjectOfType<CurrencyController>();

        StartCoroutine(check_health_state());// monitore health
        StartCoroutine(vizualize_health_condition());
    }

    public override void refresh() // this is called from gamemanager
    {

        base.refresh();

        deserilize_object();

    }




    [SerializeField]
    string player_prefs_key = "helth_key";



    [SerializeField]
    HealthManagerDatabase health_manager_database;


    public enum Lion_states { sad, neutral, happy, count };
    public Lion_states lion_state;

    public float Current_sleep
    {
        get
        {
            return health_manager_database.sleep;
        }
        set
        {
            health_manager_database.sleep = value;
        }
    }

    public float Current_water
    {
        get
        {
            return health_manager_database.water;
        }
        set
        {
            health_manager_database.water = value;
        }
    }



    public float Current_food
    {
        get
        {
            return health_manager_database.food;
        }
        set
        {
            health_manager_database.food = value;
        }
    }





    public float Petting
    {
        get
        {
            return health_manager_database.petting;
        }
        set
        {
            health_manager_database.petting = value;
        }
    }

    public Lion_states Lion_state
    {
        get
        {
            return lion_state;
            //return float_to_Lion_states();
        }
        set
        {
            lion_state = value;
        }

    }

    public HealthManagerDatabase Health_manager_database
    {
        get
        {
            return health_manager_database;
        }
        set
        {
            health_manager_database = value;
        }
    }


    IEnumerator check_health_state()
    {
        while (true)
        {

            //     lion_baring =
            //    ((game_controller.currency_setisfaction_factor
            //     * currancy_controller.Last_out_going_transaction_amount) /
            //     currancy_controller.Last_out_going_transaction_time * simulation_speed) + Petting;
            //     Debug.Log(currancy_controller.Last_out_going_transaction_amount + " " + currancy_controller.Last_out_going_transaction_time + " " + Petting);

            lion_baring = Mathf.Max(0, lion_baring - 1 + Petting);

            if (50 > lion_baring)
            {
                Lion_state = Lion_states.sad;
                // make a complaynt
            }
            else
            {
                Lion_state = Lion_states.happy;
            }
            //todo: add neutral level
            is_health_state_checked = true; // todo:  optimisation

            yield return new WaitForSeconds(health_check_rate_in_seconds);
        }
    }


    IEnumerator vizualize_health_condition() //shows health condition to user
    {
        while (true)
        {
            if (is_health_state_checked) //check if monitorrig coroutine has checked state
                switch (lion_state)
                {
                    case Lion_states.sad:

                        animation_manager.sad_animation();

                        break;

                    case Lion_states.happy:

                        animation_manager.happy_animation();

                        break;

                    case Lion_states.neutral:

                        animation_manager.neutral_animation();
                        break;
                }

            yield return new WaitForSeconds(vizualize_health_condition_rate);
        }
    }


    public string feed(float product_value) // product_value is multyple of amount and wheight
    {

        if (Current_food <= maximum_food_limit)
        {
            Current_food = product_value;
            Petting += product_value;
            animation_manager.feed_animation();
            return "successfully fed";
        }
        else
        {
            return "no more food";
        }

    }
    public string water(float product_value)
    {
        if (Current_water <= maximum_water_limit)
        {
            Current_water = product_value;

            Petting += product_value;
            animation_manager.water_animation();
            return "successfully got water";
        }
        else
        {
            return "no more water";
        }
    }

    public string sleep(float product_value)
    {
        if (Current_sleep <= maximum_sleep_limit)
        {
            Current_sleep = product_value;
            Petting += product_value;
            animation_manager.sleep_animation();
            return "successfully slept";
        }
        else
        {
            return "no more sleep";
        }
    }


    public void serilize_object(HealthManagerDatabase currency_controller_database)
    {

        string json_object = JsonUtility.ToJson(currency_controller_database);
        PlayerPrefs.SetString(player_prefs_key, json_object);

    }


    // Lion_states float_to_Lion_states()
    //{
    // return ((Lion_states)(Current_helth / 101f * (int)Lion_states.count));
    //}




    void deserilize_object()
    {

        string json_string = PlayerPrefs.GetString(player_prefs_key);

        if (string.IsNullOrEmpty(json_string)) //first time game starts initilaze with z s
        {
            health_manager_database = new HealthManagerDatabase();
        }
        else // get object from local machine
        {
            health_manager_database = JsonUtility.FromJson<HealthManagerDatabase>(json_string);
        }


    }



    [System.Serializable]
    public class HealthManagerDatabase
    {


        public float water, food, sleep;
        public float petting;

    }




}
