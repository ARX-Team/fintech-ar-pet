﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    #region publicFileds
    public float currency_conversion_rate;

    public float currency_setisfaction_factor;

    #endregion

    #region  serilizedFileds
    [SerializeField]
    string player_prefs_key = "player_prefs_key";


    [SerializeField]
    FintechManager fintech_manager;

    [SerializeField]
    CurrencyController currency_controller;

    [SerializeField]
    AnimationMager animation_manager;

    [SerializeField]
    InputManager input_manager;

    [SerializeField]
    SkinManager skin_manager;

    [SerializeField]
    SoundManager sound_manager;

    [SerializeField]
    HealthManager health_manager;

    [SerializeField]
    ApiManager api_manager;

    [SerializeField]
    InventoryManager inventory_manager;

    #endregion



    void Awake()
    {
     
        DontDestroyOnLoad(gameObject); // this script is attached to the root object

        initilize_game();

        PlayerPrefs.DeleteAll();
    }


    private void initilize_game()
    {
        animation_manager.initilize();
        currency_controller.initilize();
        input_manager.initilize();
        skin_manager.initilize();
        sound_manager.initilize();
        health_manager.initilize();
        inventory_manager.initilize();
        api_manager.initilize();

        fintech_manager.GetDepositInfo("session", 1, (depo)=>{
            if(!api_manager.api_request.is_request_done)
            {
                api_manager.api_request.is_request_done = true;
                Debug.Log(depo.CurrentBalance);
                currency_controller.Current_real_money = (float)depo.CurrentBalance;
                currency_controller.serilize_object();
                refresh_game();
            }
        });

    }

    private void refresh_game()
    {
        animation_manager.refresh();
        currency_controller.refresh();
        input_manager.refresh();
        skin_manager.refresh();
        sound_manager.refresh();
        health_manager.refresh();
        inventory_manager.refresh();
        api_manager.refresh();
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R)) // for testing
        {
            refresh_game();
        }
       
    }








}
