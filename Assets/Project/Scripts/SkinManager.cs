﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinManager : ControllerBase
{



    public override void initilize() // this is called from gamemanager
    {
        base.initilize();
        deserilize_object();

    }

    public override void refresh() // this is called from gamemanager
    {

        base.refresh();

        deserilize_object();
    }


    [SerializeField]
    string player_prefs_key = "helth_key";

    [SerializeField]
    SkinManagerDatabase skin_manager_database;





    public SkinManagerDatabase Current_helth
    {
        get
        {
            return skin_manager_database;
        }
        set
        {
            skin_manager_database = value;
        }
    }

    public int Lion_state
    {
        get
        {
            return skin_manager_database.current_skin;
        }
        set
        {
            skin_manager_database.current_skin = value;
        }
    }

    public SkinManagerDatabase Currency_controller
    {
        get
        {
            return skin_manager_database;
        }
        set
        {
            skin_manager_database = value;
        }
    }


    void serilize_object(SkinManagerDatabase currency_controller_database)
    {
        string json_object = JsonUtility.ToJson(currency_controller_database);
        PlayerPrefs.SetString(player_prefs_key, json_object);

    }


    void deserilize_object()
    {
        string json_string = PlayerPrefs.GetString(player_prefs_key);

        if (string.IsNullOrEmpty(json_string)) //first time game starts initilaze with z s
        {
            skin_manager_database = new SkinManagerDatabase();
        }
        else // get object from local machine
        {
            skin_manager_database = JsonUtility.FromJson<SkinManagerDatabase>(json_string);

        }


    }


    [System.Serializable]
    public class SkinManagerDatabase
    {
        public int current_skin;

    }



}
