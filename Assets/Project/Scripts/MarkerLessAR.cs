﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VR;

public class MarkerLessAR : MonoBehaviour
{

    private WebCamTexture cam;
    public RawImage background;
    public AspectRatioFitter fit;

    private bool arReady = false;
    private Quaternion baseRotation;
    Color[] colors;
    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        Application.targetFrameRate = 30;
        // if(ge.arx.chemistry.ChemistryManager.Instance.VRMode)
        //     enabled = false;

    }

    /// <summary>
    /// This function is called when the MonoBehaviour will be destroyed.
    /// </summary>
    void OnDisable()
    {
        if(cam != null){
            cam.Stop();
            Destroy(cam);
        }
        
    }

    /// <summary>
    /// This function is called when the object becomes enabled and active.
    /// </summary>
    void OnEnable()
    {
        StartCoroutine(StartO());
    }


    // Use this for initialization
    IEnumerator StartO()
    {

        // for (int i = 0; i < WebCamTexture.devices.Length; i++)
        // {
        //     if (!WebCamTexture.devices[i].isFrontFacing)
        //     {
        //         cam = new WebCamTexture(WebCamTexture.devices[i].name, Screen.width, Screen.height);
        //         break;
        //     }
        // }
        cam = new WebCamTexture(Screen.height, Screen.height , 30);
        // cam = new WebCamTexture();

        if (cam == null)
        {
            Debug.LogError("Couldn't find Camera");
            yield break;
        }else
            Debug.LogError("Found Camera " + cam.deviceName);

        
        // cam.requestedFPS = 60f;
        SetCamera(true);
        // background.texture = cam;
        background.material.mainTexture = cam;


        while(cam.width < 100)
            yield return null;

        arReady = true;
        float ratio = (float)cam.width / (float)cam.height;
        fit.aspectRatio = ratio;

        // float scaleY = cam.videoVerticallyMirrored ? -1.0f : 1.0f;
        // background.rectTransform.localPosition = new Vector3(1f, scaleY, 1f);

        if ( cam.videoVerticallyMirrored )
            background.uvRect = new Rect(1,0,1,-1);  // means flip on vertical axis
        else
            background.uvRect = new Rect(0,0,1,1);  // means no flip

        int orient = -cam.videoRotationAngle;
        background.rectTransform.localEulerAngles = Vector3.forward * orient;
    }
	public void SetCamera(bool active){
		if(active){
			cam.Play();
		}else{
			if(cam.isPlaying)
				cam.Pause();
		}
		background.gameObject.SetActive(active);
	}

}
