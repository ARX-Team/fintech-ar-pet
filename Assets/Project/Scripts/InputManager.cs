﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : ControllerBase
{
    public override void initilize() // this is called from gamemanager
    {
        base.initilize();
    }

    public override void refresh() // this is called from gamemanager
    {

        base.refresh();
    }


    public delegate void On_object_click(GameObject caller);
    public static event On_object_click _on_object_click;



    [SerializeField]
    int touch_count;

    [SerializeField]
    GameObject camera_object;

    [SerializeField, Range(21, 140)]
    float zoom_speed;

    [SerializeField]
    GameObject lion_object;

    private Ray ray;
    private RaycastHit hit;
    private void Update()
    {

        touch_count = Input.touchCount;


        if (Input.GetMouseButtonDown(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100000))
            {
                GameObject hit_object = hit.transform.gameObject;
                if (hit_object != null)
                {
                    if (_on_object_click != null)
                    {
                        _on_object_click(hit_object);
                    }
                    else
                    {
                        Debug.Log("_on_object_click has no subscribers");
                    }

                }


            }
        }

        switch (touch_count)
        {
            case 1:
                raycaster();
                break;

            case 2:
                zoom();
                break;
        }

    }


    private void raycaster()
    {
        ray = Camera.main.ScreenPointToRay(Input.touches[0].position);

        if (Physics.Raycast(ray, out hit, 100000))
        {
            GameObject hit_object = hit.transform.gameObject;
            if (hit_object != null)
            {
                if (_on_object_click != null)
                {
                    _on_object_click(hit_object);
                }
                else
                {
                    Debug.Log("_on_object_click has no subscribers");
                }

            }


        }

    }

    private void zoom()
    {

        Touch touchZero = Input.GetTouch(0);
        Touch touchOne = Input.GetTouch(1);

        Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
        Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

        float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
        float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
        float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

        camera_object.GetComponent<Camera>().fieldOfView += deltaMagnitudeDiff * zoom_speed;
        camera_object.GetComponent<Camera>().fieldOfView = Mathf.Clamp(camera_object.GetComponent<Camera>().fieldOfView, 0.1f, 179.9f);

    }





}
