﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : ControllerBase
{


    [SerializeField]
    string player_prefs_key = "player_prefs_key";

    [SerializeField]
    Inventory_item_list inventory_iten_list;

    [SerializeField]
    AnimationMager animation_manager;

    [SerializeField]
    HealthManager health_manager;
    public override void initilize()
    {
        base.initilize();
        animation_manager = FindObjectOfType<AnimationMager>();
        health_manager = FindObjectOfType<HealthManager>();

        deserilize_object(); // get list of items inventory basicaly
    }


    public override void refresh()
    {
        base.refresh();

        deserilize_object(); // get list of items inventory basicaly
    }


    public void gift(treets tr, float amount)
    {

        string gift_result; // contains response from lion
        switch (tr)
        {
            case treets.food:

                gift_result = health_manager.feed(amount);
                break;
            case treets.water:

                gift_result = health_manager.water(amount);

                break;
            case treets.sleep:

                gift_result = health_manager.sleep(amount);

                break;
        }

    }

    private void Update()// for testting
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            gift(treets.food, 10);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            gift(treets.water, 10);
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            gift(treets.sleep, 10);
        }
    }


    public void serilize_object(Inventory_item_list currency_controller_database)
    {
        string json_object = JsonUtility.ToJson(currency_controller_database);
        PlayerPrefs.SetString(player_prefs_key, json_object);

    }




    void deserilize_object()
    {
        string json_string = PlayerPrefs.GetString(player_prefs_key);

        if (string.IsNullOrEmpty(json_string)) //first time game starts initilaze with z s
        {
            inventory_iten_list = new Inventory_item_list();
        }
        else // get object from local machine
        {
            inventory_iten_list = JsonUtility.FromJson<Inventory_item_list>(json_string);

        }

    }
}

public enum treets { water, sleep, food }

[System.Serializable]
public class Inventory_item
{
    public treets treet;
    public int setisfaction_time;
    public float price_in_game_curreny;
}


[System.Serializable]
public class Inventory_item_list
{
    public List<Inventory_item> inventory_list;
}




