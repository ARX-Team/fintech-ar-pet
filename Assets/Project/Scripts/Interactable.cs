﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{

    [SerializeField]
    Animator animator;

    [SerializeField]
    HealthManager health_manager;

    private void Awake()
    {
        health_manager = FindObjectOfType<HealthManager>();



        if (GetComponent<Animation>())
        {
            animator = GetComponent<Animator>();
        }
        else
        {
            Debug.Log("animator is not attached: " + gameObject.name);
        }
    }

    public void on_click()
    {

        AnimatorControllerParameter[] paramiters = GetComponent<Animator>().parameters;



        foreach (AnimatorControllerParameter param in paramiters)
        {

            GetComponent<Animator>().ResetTrigger(param.name);

            switch (health_manager.lion_state)
            {
                case HealthManager.Lion_states.happy:

                    GetComponent<Animator>().SetTrigger("Happy");
                    break;

                case HealthManager.Lion_states.neutral:
                    //GetComponent<Animator>().SetTrigger("Happy");

                    break;

                case HealthManager.Lion_states.sad:
                    GetComponent<Animator>().SetTrigger("Sad");

                    break;

            }


        }


    }




    public void on_skin_changed()
    {

    }





}
