﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApiManager : ControllerBase
{

    [SerializeField]
    FintechManager fintech_manager;

    [SerializeField]
    public IpiRequest api_request;

    [SerializeField]
    string player_prefs_key = "ApiManager";


    public override void initilize() // this is called from gamemanager
    {
        base.initilize();
        deserilize_object();

    }

    public override void refresh() // this is called from gamemanager
    {

        base.refresh();

        deserilize_object();
    }


    private void Awake()
    {
        base.initilize();

        deserilize_object();


        if (api_request.is_request_done)
        {
            // don't make a requets
        }
        else
        {
            // make a reqesu
        }

    }



    public void serilize_object(IpiRequest currency_controller_database)
    {

        string json_object = JsonUtility.ToJson(currency_controller_database);
        PlayerPrefs.SetString(player_prefs_key,json_object);

    }





    void deserilize_object()
    {

        string json_string = PlayerPrefs.GetString(player_prefs_key);

        if (string.IsNullOrEmpty(json_string)) //first time game starts initilaze with z s
        {
            api_request = new IpiRequest();
        }
        else // get object from local machine
        {
            api_request = JsonUtility.FromJson<IpiRequest>(json_string);
        }

    }


    [System.Serializable]
    public class IpiRequest
    {
        public bool is_request_done;


    }


}
