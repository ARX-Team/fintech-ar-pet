﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationMager : ControllerBase
{

    [SerializeField]
    float default_animation_refresh_time;

    [SerializeField]
    GameObject lion_object;

    [SerializeField]
    GameObject foodParticle;

    [SerializeField]
    GameObject waterParticle;


    [SerializeField]
    string last_animation;

    [SerializeField]
    HealthManager health_manager;

    #region enableDisbale
    private void OnEnable()
    {
        InputManager._on_object_click += on_click;

    }

    private void OnDisable()
    {
        InputManager._on_object_click -= on_click;
    }

    #endregion

    public override void initilize() // this is called from gamemanager
    {
        base.initilize();


        StartCoroutine(refresh_default_animation());


    }

    public override void refresh() // this is called from gamemanager
    {

        base.refresh();

    }

    void reset_anim(string last_anim)
    {
        lion_object.GetComponent<Animator>().ResetTrigger(last_animation);
        last_animation = last_anim;
        lion_object.GetComponent<Animator>().SetTrigger(last_animation);
    }

    public void feed_animation()
    {
        reset_anim("Feed");
        Destroy(Instantiate(foodParticle, lion_object.transform), 2);
    }

    public void sleep_animation()
    {
        reset_anim("Sleep");
        Debug.Log("sleep animation");

    }


    public void water_animation()
    {
        reset_anim("Feed");
        Destroy(Instantiate(waterParticle, lion_object.transform), 2);

        Debug.Log("water animation");
    }

    public void sad_animation()
    {
        Debug.Log("lion is sad");
    }

    public void happy_animation()
    {
        reset_anim("Happy"); ;
    }

    public void neutral_animation()
    {
        Debug.Log("lion is neutral");
    }


    IEnumerator refresh_default_animation()
    {
        while (true)
        {
            call_animation();
            yield return new WaitForSeconds(default_animation_refresh_time);
        }

    }


    public void on_click(GameObject caller)
    {

        if (caller != lion_object) return;

        call_animation();



        //  if (caller.GetComponent<Interactable>())
        //  {
        //      caller.GetComponent<Interactable>().on_click();
        //  }


    }

    void call_animation()
    {


        switch (health_manager.lion_state)
        {
            case HealthManager.Lion_states.happy:
                reset_anim("Happy");
                break;

            case HealthManager.Lion_states.neutral:
                // GetComponent<Animator>().SetTrigger("Happy");

                break;

            case HealthManager.Lion_states.sad:
                reset_anim("Sad");
                break;

        }



    }


}
