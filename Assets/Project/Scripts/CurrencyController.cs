﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CurrencyController : ControllerBase
{



    [SerializeField]
    GameController game_controller;

    [SerializeField]
    HealthManager health_manager;

    public override void initilize() // this is called from gamemanager
    {
        base.initilize();

        game_controller = FindObjectOfType<GameController>();

        deserilize_object();

    }

    public override void refresh() // this is called from gamemanager
    {

        base.refresh();

        deserilize_object();

    }


    [SerializeField]
    string player_prefs_key = "currency_key";

    [SerializeField]
    CurrencyControllerDatabase currency_controller_database;



    public int Current()
    {
        DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        int currentEpochTime = (int)(DateTime.UtcNow - epochStart).Seconds;
        return currentEpochTime;
    }

    public int SecondsElapsed(int t1)
    {
        int difference = Current() - t1;
        return difference%3600;
    }

    public int SecondsElapsed(int t1, int t2)
    {
        int difference = t1 - t2;
        return Mathf.Abs(difference);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            make_paymant(100);
        }
    }

    public void make_paymant(float amount_in_real_money) // set varibles
    {
        Current_real_money += amount_in_real_money;
        Last_out_going_transaction_time = Current();
        Last_out_going_transaction_amount = amount_in_real_money;

        //zero out values
        health_manager.Health_manager_database = new HealthManager.HealthManagerDatabase();

        // save to local machine
        serilize_object();
    }

    public float Current_real_money
    {
        get
        {
            return currency_controller_database.current_real_money;
        }
        set
        {
            //set real money
            currency_controller_database.current_real_money = value;

            //set ingame currency
            Ingame_currency = value *
            game_controller.currency_conversion_rate;
        }
    }


    public float Ingame_currency
    {
        get
        {
            return currency_controller_database.current_ingame_money;
        }
        set
        {
            currency_controller_database.current_ingame_money = value;
        }
    }


    public CurrencyControllerDatabase Currency_controller
    {
        get
        {
            return currency_controller_database;
        }
        set
        {
            currency_controller_database = value;
        }
    }


    public float Last_incoming_transaction_time
    {
        get
        {
            return currency_controller_database.last_incoming_transaction_time;
        }
        set
        {
            currency_controller_database.last_incoming_transaction_time = value;
        }
    }


    public float Last_incoming_transaction_amount
    {
        get
        {
            return currency_controller_database.last_outgoing_transaction_amount;
        }
        set
        {
            currency_controller_database.last_outgoing_transaction_amount = value;
        }
    }


    public int Last_out_going_transaction_time
    {
        get
        {
            return SecondsElapsed(currency_controller_database.last_out_going_transaction_time);
        }
        set
        {
            currency_controller_database.last_out_going_transaction_time = value;
        }
    }

    public float Last_out_going_transaction_amount
    {
        get
        {
            return currency_controller_database.last_incomming_transaction_amount;
        }
        set
        {
            currency_controller_database.last_incomming_transaction_amount = value;
        }
    }




    public void serilize_object()
    {
        string json_object = JsonUtility.ToJson(currency_controller_database);
        PlayerPrefs.SetString(player_prefs_key, json_object);

    }


    void deserilize_object()
    {
        string json_string = PlayerPrefs.GetString(player_prefs_key);


        if (string.IsNullOrEmpty(json_string)) //first time game starts initilaze with z s
        {
            currency_controller_database = new CurrencyControllerDatabase();
        }
        else // get object from local machine
        {
            currency_controller_database = JsonUtility.FromJson<CurrencyControllerDatabase>(json_string);
        }

    }


    [System.Serializable]
    public class CurrencyControllerDatabase
    {
        public float current_real_money;
        public float current_ingame_money;
        public float last_outgoing_transaction_amount;
        public float last_incomming_transaction_amount;
        public float last_incoming_transaction_time;
        public int last_out_going_transaction_time;
    }





}
